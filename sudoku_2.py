import argparse
import sys
from copy import deepcopy
import datetime
import csv

class SudokuGrid:
    size = 3
    def __init__(self, line):
        self.line = line

# 004300209005009001070060043
# 006002087190007400050083000
# 600000105003508690042910300

#  |  |  |  |  |  |  |  |  |
#  V  V  V  V  V  V  V  V  V

#   | 0 1 2 | 3 4 5 | 6 7 8 |
#---+-------+-------+-------+
# 0 | 0 0 4 | 3 0 0 | 2 0 9 |
# 1 | 0 0 5 | 0 0 9 | 0 0 1 |
# 2 | 0 7 0 | 0 6 0 | 0 4 3 |
#---+-------+-------+-------+
# 3 | 0 0 6 | 0 0 2 | 0 8 7 |
# 4 | 1 9 0 | 0 0 7 | 4 0 0 |
# 5 | 0 5 0 | 0 8 3 | 0 0 0 |
#---+-------+-------+-------+
# 6 | 6 0 0 | 0 0 0 | 1 0 5 |
# 7 | 0 0 3 | 5 0 8 | 6 9 0 |
# 8 | 0 4 2 | 9 1 0 | 3 0 0 |

    @property
    def squares(self):
        squares = []
        for y in range(SudokuGrid.size):
            for x in range(SudokuGrid.size):
                # square upper-left corner at position (x, y)
                square = set()
                for shift_x in range(SudokuGrid.size):
                    for shift_y in range(SudokuGrid.size):
                        element = int(self.line[x*SudokuGrid.size + shift_x + y*(SudokuGrid.size**3) + shift_y*(SudokuGrid.size**2)])
                        if not element:
                            continue
                        if element in square:
                            raise ValueError
                        square.add(element)
                squares.append(sorted(list(square)))
        return squares

    def square_for_ind(self, ind):
        x, y = self.convert_ind_to_xy(ind)
        square_ind = int(y/SudokuGrid.size) * SudokuGrid.size + int(x/SudokuGrid.size)
        return self.squares[square_ind]

    @property
    def rows(self):
        rows = []
        for j in range(SudokuGrid.size**2):
            row_subline = self.line[j*(SudokuGrid.size**2):(j + 1)*(SudokuGrid.size**2)]
            row = set()
            for element in row_subline:
                element = int(element)
                if not element:
                    continue
                if element in row:
                    raise ValueError
                row.add(element)
            rows.append(sorted(list(row)))
        return rows

    def row_for_ind(self, ind):
        _, y = self.convert_ind_to_xy(ind)
        return self.rows[y]

    @property
    def columns(self):
        columns = []
        for i in range(SudokuGrid.size**2):
            column = set()
            for j in range(SudokuGrid.size**2):
                element = int(self.line[i + j*(SudokuGrid.size**2)])
                if not element:
                    continue
                if element in column:
                    raise ValueError
                column.add(element)
            columns.append(sorted(list(column)))
        return columns

    def column_for_ind(self, ind):
        x, _ = self.convert_ind_to_xy(ind)
        return self.columns[x]

    def convert_ind_to_xy(self, ind):
        line_length = SudokuGrid.size**2
        x = int(ind % line_length)
        y = int(ind / line_length)
        return x, y

    @property
    def is_solved(self):
        try:
            for row in self.rows:
                if len(row) < SudokuGrid.size**2:
                    return False
            for column in self.columns:
                if len(column) < SudokuGrid.size**2:
                    return False
            for square in self.squares:
                if len(square) < SudokuGrid.size**2:
                    return False
        except Exception:
            print("Impossible grid.")
            return False
        return True

    def __repr__(self):
        s = ""
        for i in range(SudokuGrid.size**2):
            s += " ".join(map(str, self.line[i*SudokuGrid.size**2:(i+1)*SudokuGrid.size**2]))
            s += "\n"
        return s


class SudokuSolver:
    def __init__(self, size):
        self.expected = list(range(1, size**2 + 1))

    def solve(self, input_grid):
        """
        `return` SudokuGrid grid, None if there is at least one cell
        with no possible values"""
        working_grid = deepcopy(input_grid)
        while True:
            possible_substitutions = self.prepare_dict_of_possible_substitution_for_cells(working_grid)
            if not possible_substitutions.keys():
                return working_grid

            is_obvious_substitution_found = False
            for working_cell_ind, working_cell_possible_values in possible_substitutions.items():
                if len(working_cell_possible_values) == 1:
                    is_obvious_substitution_found = True
                    working_grid.line[working_cell_ind] = list(working_cell_possible_values)[0]
            if not is_obvious_substitution_found:
                break

        possible_substitutions = self.prepare_dict_of_possible_substitution_for_cells(working_grid)
        sorted_substitutions = sorted(list(possible_substitutions.items()), key=lambda x: len(x[1]))
        working_cell_ind, working_cell_possible_values = sorted_substitutions[0]
        if not working_cell_possible_values:
            return None

        for possible_value in working_cell_possible_values:
            working_grid.line[working_cell_ind] = possible_value
            probably_solved_grid = self.solve(working_grid)
            if probably_solved_grid and probably_solved_grid.is_solved:
                return probably_solved_grid

    def prepare_dict_of_possible_substitution_for_cells(self, sudoku_grid):
        possible_values = {}
        for ind, cell_value in enumerate(sudoku_grid.line):
            if int(cell_value):
                continue
            try:
                possible_values[ind] = set(self.expected) \
                        - set(sudoku_grid.row_for_ind(ind)) \
                        - set(sudoku_grid.column_for_ind(ind)) \
                        - set(sudoku_grid.square_for_ind(ind))
            except:
                return {}
        return possible_values


def main(args):
    with open(args.input_file, "r", newline='') as input_f:
        d_reader = csv.DictReader(input_f)

        start_time = datetime.datetime.now()
        count = 0
        for row in d_reader:
            count += 1
            sudoku_grid = SudokuGrid(line=list(map(int, row["quizzes"])))

            s_solver = SudokuSolver(size=SudokuGrid.size)
            solved_grid = s_solver.solve(sudoku_grid)

            if ''.join(map(str, solved_grid.line)) != row["solutions"]:
                print(row["quizzes"])
                print("======")
                print(sudoku_grid)
                print("======")
                print(solved_grid)
                print("======")
                print(row["solutions"])
                return

        end_time = datetime.datetime.now()
        total_time = end_time - start_time
        print(f"sudoku solved: {count}")
        print(f"time spent: {total_time}")
        print(f"average time: {total_time / count}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_file",
    )

    args = parser.parse_args()
    sys.exit(main(args))
